package com.example.pwe.firestoretestproject

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.pwe.firestoretestproject.Common.Companion.USER_ID
import com.example.pwe.firestoretestproject.Common.Companion.USER_NAME
import kotlinx.android.synthetic.main.login_activity.*

@SuppressLint("Registered")
class LogInActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)

        buttonLogIn.setOnClickListener {
            editTextUserName.text
            editTextUserId.text

            var intent = Intent(this@LogInActivity, ChatActivity::class.java)
            intent.putExtra(USER_ID, editTextUserId.text.toString())
            intent.putExtra(USER_NAME, editTextUserName.text.toString())
            startActivity(intent)
            finish()
        }
    }
}