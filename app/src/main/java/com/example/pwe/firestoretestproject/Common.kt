package com.example.pwe.firestoretestproject

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*

class Common {


    @SuppressLint("SimpleDateFormat")
    fun getGMTTime(): ZonedDateTime? {
//        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
//                Locale.getDefault())
//        dateFormat.timeZone = TimeZone.getTimeZone("GMT")
//        return dateFormat

        val londonZone = ZoneId.of("GMT")
        val londonCurrentDateTime = ZonedDateTime.now(londonZone)
        println(londonCurrentDateTime)
        return londonCurrentDateTime
    }

    companion object {
        const val USER_NAME = "user_name"
        const val USER_ID = "user_id"
    }
}