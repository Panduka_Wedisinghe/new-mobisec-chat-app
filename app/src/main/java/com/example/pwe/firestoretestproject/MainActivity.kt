package com.example.pwe.firestoretestproject

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {


    companion object {
        lateinit var firestore: FirebaseFirestore
            private set
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        firestore = FirebaseFirestore.getInstance()

        var textQuote = editTextQuote
        var textAuthor = editTextAuthor

        buttonSave.setOnClickListener {

        }
        buttonSave.setOnClickListener {
            val user = HashMap<String, String>()
            user.put("quote", textQuote.toString())
            user.put("author", textAuthor.toString())


            //Test
            firestore.collection("Quotes2")
                .add(user as Map<String, Any>)
                .addOnSuccessListener {
                    Log.d("SUCCESS", "Write")
                }
                .addOnFailureListener {
                    Log.d("FAILURE", "Write")
                }

//            firestore.collection("Quotes").document("inspiration")
//                .set(user as Map<String, Any>)
//                .addOnSuccessListener {
//                    Log.d("SUCCESS", "Write")
//
//                }.addOnFailureListener {
//                    Log.d("FAILURE", "Write")
//                }
        }

        buttonRetrieve.setOnClickListener {
            firestore.collection("Quotes").document("inspiration")
                .get()
                .addOnCompleteListener {
                    Log.d("SUCCESS", "Retrieve")
                }
                .addOnFailureListener {
                    Log.d("Failure", "Retrieve")
                }
        }
    }
}
