package com.example.pwe.firestoretestproject

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.firestore.DocumentSnapshot
import kotlinx.android.synthetic.main.receiver_message.view.*
import kotlinx.android.synthetic.main.sender_message.view.*

class MessagesAdapter(var context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var list: List<DocumentSnapshot> = ArrayList()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.sender_message, p0, false)
        when (p1) {
            1 -> {
                val v = LayoutInflater.from(p0.context).inflate(R.layout.sender_message, p0, false)
                return ViewHolderSender(v)
            }
            2 -> {
                val v = LayoutInflater.from(p0.context).inflate(R.layout.receiver_message, p0, false)
                return ViewHolderReceiver(v)
            }
        }
        return ViewHolderSender(v)
    }

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        when (p0.itemViewType) {
            1 -> {
                val viewHolder:ViewHolderSender = p0 as ViewHolderSender
                viewHolder.bind(context,list[p1])
            }
            2 -> {
                val viewHolder:ViewHolderReceiver = p0 as ViewHolderReceiver
                viewHolder.bind(context,list[p1])
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        val l = list[position]
        when (l.data!!["Id"]) {
            "1" -> {
                return 1
            }
            "2" -> {
                return 2
            }
        }
        return super.getItemViewType(position)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setData(documents: List<DocumentSnapshot>) {
        list = documents
    }

    class ViewHolderReceiver(view: View) : RecyclerView.ViewHolder(view) {
        private var receiverMessage = view.textViewReceiver
        private var timeStamp = view.text_message_receiver_time
        fun bind(context: Context, documentSnapshot: DocumentSnapshot) {
            receiverMessage.text = documentSnapshot.data!!["Message"].toString()
            timeStamp.text = documentSnapshot.data!!["Timestamp"].toString()
        }

    }

    class ViewHolderSender(view: View) : RecyclerView.ViewHolder(view) {
        private var senderMessage = view.textViewSender
        fun bind(context: Context, documentSnapshot: DocumentSnapshot) {
            senderMessage.text = documentSnapshot.data!!["Message"].toString()
        }

    }
}