package com.example.pwe.firestoretestproject

import android.support.v7.app.AppCompatActivity
import android.widget.Toast

open class BaseActivity:AppCompatActivity() {

    fun toastMessages(message:String){
        Toast.makeText(this,message,Toast.LENGTH_LONG).show()
    }
}