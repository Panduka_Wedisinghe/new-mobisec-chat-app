package com.example.pwe.firestoretestproject

import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.example.pwe.firestoretestproject.Common.Companion.USER_ID
import com.example.pwe.firestoretestproject.Common.Companion.USER_NAME
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_chat.*
import java.util.*


class ChatActivity : BaseActivity() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var firestore: FirebaseFirestore
            private set

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        val extras = intent.extras
        val id = extras.get(USER_ID).toString()
        val name: String = extras.get(USER_NAME).toString()
        recyclerViewChats.layoutManager = LinearLayoutManager(this@ChatActivity,
                RecyclerView.VERTICAL, false)
        recyclerViewChats.hasFixedSize()

        val adapter = MessagesAdapter(this@ChatActivity)
        recyclerViewChats.adapter = adapter

        firestore = FirebaseFirestore.getInstance()

        buttonSend.setOnClickListener {
            if (editTextTypedMessage.text.toString().isEmpty()) {
                toastMessages("Can't send empty messages")
            } else {
                sendMessage(name, id)
            }
        }

        firestore.collection("Chats")
                .orderBy("Timestamp", Query.Direction.ASCENDING)
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        for (document in task.result!!) {
                            Log.d(TAG, document.id + " => " + document.data)
                        }
                    } else {
                        Log.w(TAG, "Error getting documents.", task.exception)
                    }
                }

        firestore.collection("Chats").orderBy("Timestamp", Query.Direction.ASCENDING)
                .addSnapshotListener { querySnapshot, _ ->
                    var size = querySnapshot?.documents?.size
                    for (i in querySnapshot?.documents!!) Log.d("Message", i.data!!["Message"].toString())
                    adapter.setData(querySnapshot.documents)
                    adapter.notifyDataSetChanged()
                    size?.let { recyclerViewChats.scrollToPosition(it) }
                }
    }

    private fun sendMessage(name: String, id: String) {
        val time = Common().getGMTTime()
        val user = HashMap<String, String>()
        user["Sender"] = name
        user["Id"] = id
        user["Message"] = editTextTypedMessage.text.toString()
        user["Timestamp"] = time.toString()

        firestore.collection("Chats")
                .add(user as Map<String, Any>)
                .addOnSuccessListener {
                    editTextTypedMessage.setText("")
                }
                .addOnFailureListener {
                    Log.d("FAILURE", "Write")
                }
    }

}

